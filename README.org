#+TITLE: CSS for Texinfo


* Summary

This repository contains the source files of my custom [[https://www.w3.org/Style/CSS/][CSS]] for [[https://www.w3.org/html/][HTML]]
documents generated with [[https://www.gnu.org/software/texinfo/][GNU Texinfo]].

The current development version can be seen applied to the [[https://luis-felipe.gitlab.io/texinfo-css/][online
documentation]].


* Copying

Public domain 2016 [[https://luis-felipe.gitlab.io/][Luis Felipe López Acevedo]]. All rights waived.

This work has been released into the public domain by its author. This
applies universewide.
