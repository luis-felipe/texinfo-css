# Running this script will build the HTML manual in the specified
# BUILD_PATH for testing the CSS.

BUILD_PATH="/tmp/texinfo-css/"
INDEX_FILE="index.html"

makeinfo --html --css-ref=static/css/texinfo-7.css \
         -c SHOW_TITLE=true -c FORMAT_MENU=menu \
         -o $BUILD_PATH main.texi
cp -R static $BUILD_PATH
echo "TEXINFO-CSS: Open file://$BUILD_PATH$INDEX_FILE in your Web browser to read the manual."
